#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "utils.h"

void
perr(char const *const message)
{
    fprintf(stderr, "***ERROR: %s: %s\n", message, errno != 0 ? strerror(errno) : "");
    fflush(stderr);
}

void
dump(uint8_t const *buffer, size_t buffer_length, char const *const format, ...)
{
    va_list args;

    va_start(args, format);
    vprintf(format, args);
    va_end(args);

    while (buffer_length--) {
        printf("%#02x ", *buffer++);
    }

    puts("");
}
