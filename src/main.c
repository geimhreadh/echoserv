#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "common.h"
#include "socket.h"
#include "utils.h"

static int volatile run;

static void
stop(void)
{
    /* This is the handler for both the 'bye' message from a peer, and SIGINT. */
    puts("stopping");
    run = 0;
}

#if defined (INSTALL_SIGINT_HANDLER)
static void
sigint_stop(int sig, siginfo_t *siginfo, void *context)
{
    UNUSED(context);
    UNUSED(siginfo);
    UNUSED(sig);
    puts("caught SIGINT");
    stop();
}
#endif

void
respond(void)
{
    size_t result;
    size_t const RX_BUFFER_LEN = 255;
    uint8_t rx_buffer[RX_BUFFER_LEN];
    char const *response = (char *)rx_buffer;
    size_t response_length;
#if defined (USE_PROTOCOL)
    struct message {
        char const *const message;
        char const *const response;
        void (*action)(void);
    } static const messages[] =  {
        { .message = "hi",  .response = "echoserv is listening\n",           .action = NULL },
        { .message = "bye", .response = "echoserv is no longer listening\n", .action = stop }
    };
#endif

    /* Read message from peer. */
    result = sock_read(rx_buffer, ARRAY_DIM(rx_buffer));
    if (result < 0) {
        goto err;
    }

    /* Chomp the message. */
    response_length = result;
    rx_buffer[response_length] = 0;
    printf("[rx]: %s", (char *)rx_buffer);

#if defined (USE_PROTOCOL)
    /* Test message for protocol, handle match appropriately. */
    for (size_t i = 0; i < ARRAY_DIM(messages); i++) {
        if (!strncmp((char const *)rx_buffer, messages[i].message, strlen(messages[i].message))) {
            response = messages[i].response;
            response_length = strlen(messages[i].response);
            if (messages[i].action) {
                messages[i].action();
            }
        }
    }
#endif

    /* Send the appropriate response to the peer. */
    result = sock_write((uint8_t *)response, response_length);
    if (result < 0) {
        goto err;
    }

    return;

err:
    perr("couldn't respond");
}

int
main(int argc, char **argv)
{
    UNUSED(argc);
    UNUSED(argv);
    int result;

#if defined(INSTALL_SIGINT_HANDLER)
    struct sigaction handler;
    memset(&handler, 0, sizeof handler);
    handler.sa_sigaction = sigint_stop;
    handler.sa_flags     = SA_SIGINFO;

    result = sigaction(SIGINT, &handler, NULL);
    if (result < 0) {
        perr("failed to install SIGINT handler");
        goto err;
    }
#endif

    puts("echoserv started");
    result = sock_init(respond);
    if (result < 0) {
        perr("socket initialisation failed");
        goto err;
    }

    run = 1;
    while (run) {
        /* Do nothing. */
    }

    sock_fini();

err:

    return 0;
}
