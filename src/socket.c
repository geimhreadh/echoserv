#include <errno.h>
#include <signal.h>
#include <stdint.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <netdb.h>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "common.h"
#include "errors.h"
#include "socket.h"
#include "utils.h"

#define LOCALHOST "127.0.0.1"
#if !defined(PORT)
#define PORT 5001
#else
#define PORT USER_PORT
#endif

static struct sock_ctx {
    int fd;
    struct sockaddr_storage peer;
    socklen_t peer_len;
} sock;

static int udp_serv_fd;
static sock_rx_init_cb rx_callback;

static void
sigio_handler(int sig, siginfo_t *siginfo, void *context)
{
    UNUSED(context);
    UNUSED(siginfo);
    UNUSED(sig);
    if (rx_callback) {
        rx_callback();
    }
}

int
sock_init(sock_rx_init_cb callback)
{
    /* Create UDP socket.
     * fcntl setting owner and O_ASYNC.
     * Install signal handler for data receipt (set callback) */
    int result;
    rx_callback = callback;

    memset(&sock.peer, 0, sizeof sock.peer);
    sock.peer_len = sizeof sock.peer;

    /* Install a signal handler for SIGIO, since we want to use asynchronous
     * I/O for data arrival. */
    struct sigaction handler;
    memset(&handler, 0, sizeof handler);
    handler.sa_sigaction = sigio_handler;
    handler.sa_flags     = SA_SIGINFO;

    result = sigaction(SIGIO, &handler, NULL);
    if (result < 0) {
        perr("signal handler installation failed");
        goto err;
    }

    /* Set up a UDP socket to receive messages from the host. */
    struct addrinfo hints,
                    *local;
    memset(&hints, 0, sizeof hints);
    hints.ai_family   = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags    = AI_PASSIVE;

    result = getaddrinfo(LOCALHOST, STR(PORT), &hints, &local);
    if (result < 0) {
        perr("failed to get address info");
        goto err;
    }

    result = socket(local->ai_family, local->ai_socktype, local->ai_protocol);
    if (result < 0) {
        perr("failed to create UDP socket");
        goto err;
    }

    udp_serv_fd = result;

    result = bind(udp_serv_fd, local->ai_addr, local->ai_addrlen);
    if (result < 0) {
        perr("failed to bind on port " STR(PORT));
        goto err;
    }

    freeaddrinfo(local);

    /* Set up asynchronous I/O for the UDP socket. */
    int flags;
    result = fcntl(udp_serv_fd, F_SETOWN, getpid());
    if (result < 0) {
        perr("failed to get set socket ownership");
        goto err;
    }

    flags = fcntl(udp_serv_fd, F_GETFL);
    if (flags < 0) {
        perr("failed to get get socket descriptor flags");
        goto err;
    }

    result = fcntl(udp_serv_fd, F_SETFL, flags | O_ASYNC);
    if (result < 0) {
        perr("failed to set socket descriptor flags");
        goto err;
    }

err:
    return result;
}

int
sock_read(uint8_t *rx_buffer, uint8_t length)
{
    int result;

    result = recvfrom(udp_serv_fd, rx_buffer, length, 0,
                      (struct sockaddr *)&sock.peer, &sock.peer_len);
    if (result < 0) {
        perr("recvfrom failed");
        goto read_done;
    }

#if defined (SOCKET_DEBUG)
    dump(rx_buffer, result, "socket rx [%02d] octets: ", result);
#endif

read_done:
    return result;
}

int
sock_write(uint8_t const *tx_buffer, uint8_t length)
{
    int result;

    result = sendto(udp_serv_fd, tx_buffer, length, 0,
                    (struct sockaddr *)&sock.peer, sock.peer_len);
    if (result < 0) {
        perr("sendto failed");
        goto write_done;
    }

#if defined (SOCKET_DEBUG)
    dump(tx_buffer, result, "socket tx [%02d] octets: ", result);
#endif

write_done:
    return result;
}

void
sock_fini(void)
{
    /* Destroy the socket. */
    close(udp_serv_fd);
}
