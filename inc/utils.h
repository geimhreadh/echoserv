#ifndef UTILS_H
#define UTILS_H

#include <stddef.h>
#include <stdint.h>

void perr(char const *const msg);

void dump(uint8_t const *buffer, size_t buffer_length, char const *const format, ...);

#endif
