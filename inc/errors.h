#ifndef LOADER_ERRORS_H
#define LOADER_ERRORS_H

#include <stdint.h>

#define SUCCESS                        0

#define ERR_CMD_READ_LENGTH           -1
#define ERR_CMD_READ_MALFORMED        -2
#define ERR_CMD_WRITE_MALFORMED       -3
#define ERR_CMD_WRITE_ZERO_LENGTH     -4
#define ERR_CMD_WRITE_LENGTH          -5
#define ERR_CMD_OPERATION_FORBIDDEN   -6

#define ERR_I2C_RX_BUFFER_SIZE        -7
#define ERR_I2C_TX_BUFFER_SIZE        -8

#define ERR_INTERLOCK_INVALID         -9
#define ERR_CMD_INTERLOCK_MALFORMED   -10

#define ERR_HAL_CALLBACK_UNDEFINED    -11

#define ERR_CMD_BOOT_MALFORMED        -12

#define ERR_HAL_WRITE_LENGTH          -13
#define ERR_HAL_WRITE_BLOCK_SPILLAGE  -14
#define ERR_HAL_READ_LENGTH           -15

#define ERR_BOOT_BVPD_INVALID         -16
#define ERR_BOOT_BVPD_NOT_FOUND       -17


#define ERR_MAX INT8_MIN

#endif
