#ifndef COMMON_H
#define COMMON_H

#define UNUSED(x) ((void)(x))

#define ARRAY_DIM(array) (sizeof array / sizeof array[0])

#define STR(s) _STR(s)
#define _STR(s) #s

#endif
