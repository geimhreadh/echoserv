#ifndef SOCKET_H
#define SOCKET_H

#include <stdint.h>

typedef void (*sock_rx_init_cb)(void);

int sock_init(sock_rx_init_cb callback);

int sock_read(uint8_t *rx_buffer, uint8_t length);

int sock_write(uint8_t const *tx_buffer, uint8_t length);

void sock_fini(void);

#endif
