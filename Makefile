BUILDROOT         = build
VARIANT          := debug

OUTPUT            = $(BUILDROOT)/echoserv
OUTPUT.elf        = $(OUTPUT).elf

DEFINES           =
DEFINES.debug     = -DSOCKET_DEBUG -DUSE_PROTOCOL
DEFINES.release   = -DNDEBUG -DINSTALL_SIGINT_HANDLER
DEFINES          += $(DEFINES.$(VARIANT))

INCLUDES          = -Iinc -I/usr/local/include
INCLUDES.release  =
INCLUDES.debug    =
INCLUDES         += $(INCLUDES.$(VARIANT))

CFLAGS           := -c -std=gnu11 -Wall -Wextra
CFLAGS.debug      = -Og -ggdb3
CFLAGS.release    = -O3
CFLAGS           += $(CFLAGS.$(VARIANT))

LIBRARIES        :=

LDFLAGS           = $(LIBRARIES)
LDFLAGS.debug     =
LDFLAGS.release   =
LDFLAGS          += $(LDFLAGS.$(VARIANT))

SRCDIRS          := src
SOURCES           = $(foreach sdir,$(SRCDIRS),$(wildcard $(sdir)/*.c))
OBJECTS           = $(patsubst %.c,$(BUILDROOT)/%.o,$(SOURCES))

.PHONY: release
release: build

.PHONY: debug
debug: build

build: $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) $(LIBRARIES) -o $(OUTPUT.elf)

.SECONDEXPANSION:
%/.fake:
	mkdir -p $(dir $@)
	touch $@

$(BUILDROOT)/%.o: %.c $$(@D)/.fake
	$(CC) $(DEFINES) $(INCLUDES) $(CFLAGS) $< -o $@

.PHONY: clean
clean:
	rm -rf build

.SUFFIXES: .c .o

.PRECIOUS: %/.fake
